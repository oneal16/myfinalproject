package com.oneeall.android.myfinalproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.oneeall.android.myfinalproject.MovieDetailActivity;
import com.oneeall.android.myfinalproject.R;
import com.oneeall.android.myfinalproject.holder.MovieViewHolder;
import com.oneeall.android.myfinalproject.modelPojo.Favorite;
import com.oneeall.android.myfinalproject.modelPojo.Movie;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oneal on 06/05/16.
 */
public class MoviesAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    private List<Movie> movieList;
    private List<Favorite> favList;
    private LayoutInflater layoutInflater;
    private Context mContext;

    public MoviesAdapter(Context context) {
        this.mContext = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.movieList = new ArrayList<>();
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.row_movie, parent, false);
        final MovieViewHolder viewHolder = new MovieViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = viewHolder.getAdapterPosition();
                Intent intent = new Intent(mContext, MovieDetailActivity.class);

                intent.putExtra(MovieDetailActivity.EXTRA_MOVIE, movieList.get(position));
                //intent.putExtra("favList", favList.get(position));


                mContext.startActivity(intent);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {

        Movie movie = movieList.get(position);
        Picasso.with(mContext).load(movie.getPoster()).placeholder(R.color.colorPrimaryDark).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return (movieList == null) ? 0 : movieList.size();
    }

    public void setMovieList(List<Movie> movieList){
        this.movieList.clear();
        this.movieList.addAll(movieList);

        notifyDataSetChanged();
    }

}
