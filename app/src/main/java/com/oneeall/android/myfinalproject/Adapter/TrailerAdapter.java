package com.oneeall.android.myfinalproject.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.oneeall.android.myfinalproject.R;
import com.oneeall.android.myfinalproject.modelPojo.Trailer;

import java.util.ArrayList;

/**
 * Created by oneal on 08/05/16.
 */
public class TrailerAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<Trailer> trailerList;

    public TrailerAdapter(Context context, int resource, ArrayList<Trailer> objects) {
        super(context, resource, objects);

        this.context = context;
        this.trailerList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_trailer, parent, false);

        final Trailer trailer = trailerList.get(position);

        TextView textView = (TextView) view.findViewById(R.id.textViewRowTrailer);
        textView.setText(trailer.getName());

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+trailer.getKey()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(intent);
            }
        });
        return view;
    }
}
