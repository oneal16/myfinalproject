package com.oneeall.android.myfinalproject.modelPojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by oneal on 06/05/16.
 */
public class Movie implements Parcelable {

    public static final String IMAGE_PATH = "http://image.tmdb.org/t/p/w500";
    public static final String IMAGE_BACKDROP_PATH = "http://image.tmdb.org/t/p/w500";


    private String title;

    @SerializedName("poster_path")
    private String poster;

    @SerializedName("overview")
    private String description;

    @SerializedName("backdrop_path")
    private String backdrop;

    @SerializedName("vote_average")
    private String user_rating;


    private String release_date;

    private int id;



    public Movie() {
    }

    protected Movie(Parcel in) {
        title = in.readString();
        poster = in.readString();
        description = in.readString();
        backdrop = in.readString();
        user_rating = in.readString();
        release_date = in.readString();
        id = in.readInt();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return IMAGE_PATH+poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBackdrop() {
        return IMAGE_BACKDROP_PATH+backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }



    public String getUser_rating() {
        return user_rating;
    }

    public void setUser_rating(String user_rating) {
        this.user_rating = user_rating;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public int getKey_id() {
        return id;
    }

    public void setKey_id(int key_id) {
        this.id = key_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(poster);
        dest.writeString(description);
        dest.writeString(backdrop);
        dest.writeString(user_rating);
        dest.writeString(release_date);
        dest.writeInt(id);
    }

    public static class MovieResult {

        private List<Movie> results;
        public List<Movie> getResults(){
            return  results;
        }
    }

}
