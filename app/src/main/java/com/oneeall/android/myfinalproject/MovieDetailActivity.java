package com.oneeall.android.myfinalproject;

import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.oneeall.android.myfinalproject.Adapter.TrailerAdapter;
import com.oneeall.android.myfinalproject.apiservice.MoviesApiService;
import com.oneeall.android.myfinalproject.modelPojo.Favorite;
import com.oneeall.android.myfinalproject.modelPojo.Movie;
import com.oneeall.android.myfinalproject.modelPojo.Trailer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MovieDetailActivity extends AppCompatActivity {

    public static final String EXTRA_MOVIE = "movie";
    public static final String EXTRA_FAV = "favorite";

    private Movie movie;
    private Favorite favorite;
    Button btn_favorited, btn_favorite;
    ImageView backdrop;
    ImageView poster;
    TextView title, description, user_rating, release_date;
    int id;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        if (getIntent().hasExtra(EXTRA_MOVIE)) {
            movie = getIntent().getParcelableExtra(EXTRA_MOVIE);

        } else if (getIntent().hasExtra(EXTRA_FAV)){
            favorite = getIntent().getParcelableExtra(EXTRA_FAV);
        }

        else {
            throw new IllegalArgumentException("Detail activity must receive a movie parcelable");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout toolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.toolbar_layout);

        if (movie != null){

            toolbarLayout.setTitle(movie.getTitle());
            backdrop = (ImageView) findViewById(R.id.backdrop);
            title = (TextView) findViewById(R.id.movie_title);
            description = (TextView)findViewById(R.id.movie_description);
            poster = (ImageView) findViewById(R.id.movie_poster);
            user_rating = (TextView) findViewById(R.id.user_rating);
            release_date = (TextView) findViewById(R.id.release_date);
            listView = (ListView) findViewById(R.id.listView);
            btn_favorited = (Button) findViewById(R.id.button_sudahFav);
            btn_favorite = (Button) findViewById(R.id.button);

            id = movie.getKey_id();

            title.setText(movie.getTitle());
            description.setText(movie.getDescription());
            user_rating.setText(movie.getUser_rating());
            release_date.setText(movie.getRelease_date());

            Picasso.with(this).load(movie.getPoster()).into(poster);
            Picasso.with(this).load(movie.getBackdrop()).into(backdrop);

        } else {


            toolbarLayout.setTitle(favorite.getTitle());
            backdrop = (ImageView) findViewById(R.id.backdrop);
            title = (TextView) findViewById(R.id.movie_title);
            description = (TextView)findViewById(R.id.movie_description);
            poster = (ImageView) findViewById(R.id.movie_poster);
            user_rating = (TextView) findViewById(R.id.user_rating);
            release_date = (TextView) findViewById(R.id.release_date);
            listView = (ListView) findViewById(R.id.listView);
            btn_favorited = (Button) findViewById(R.id.button_sudahFav);
            btn_favorite = (Button) findViewById(R.id.button);

            id = favorite.getIds();

            title.setText(favorite.getTitle());
            description.setText(favorite.getDescription());
            user_rating.setText(favorite.getUser_rating());
            release_date.setText(favorite.getRelease_date());

            Picasso.with(this).load(favorite.getPoster()).into(poster);
            Picasso.with(this).load(favorite.getBackdrop()).into(backdrop);

        }



    }

    @Override
    public void onStart(){
        super.onStart();


    }




    public void popUpTrailer(View view) {
        Intent intent = new Intent(MovieDetailActivity.this, ListTrailer.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    public void favMovie(View view) {
        Favorite favorite = new Favorite(movie.getKey_id(),movie.getTitle(),movie.getPoster(),movie.getDescription(), movie.getBackdrop(), movie.getUser_rating(), movie.getRelease_date());
        try {
            favorite.save();
            btn_favorited.setVisibility(View.VISIBLE);
            btn_favorite.setVisibility(View.GONE);
            Toast.makeText(MovieDetailActivity.this, " Fav, save !", Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            e.printStackTrace();
        }



    }
}
