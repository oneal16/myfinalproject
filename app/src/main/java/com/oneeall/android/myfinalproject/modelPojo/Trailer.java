package com.oneeall.android.myfinalproject.modelPojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by oneal on 08/05/16.
 */
public class Trailer {

    @SerializedName("id")
    private String key_id;
    private String iso_639_1;
    private String iso_3166_1;
    private String key;
    private String name;
    private String site;
    private int size;
    private String type;

    public String getKey_id() {
        return key_id;
    }

    public void setKey_id(String id) {
        this.key_id = id;
    }

    public String getIso_639_1() {
        return iso_639_1;
    }

    public void setIso_639_1(String iso_639_1) {
        this.iso_639_1 = iso_639_1;
    }

    public String getIso_3166_1() {
        return iso_3166_1;
    }

    public void setIso_3166_1(String iso_3166_1) {
        this.iso_3166_1 = iso_3166_1;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public static class TrailerResult{

        private List<Trailer> results;
        public List<Trailer> getResults() {
            return results;
        }
    }


}
