package com.oneeall.android.myfinalproject;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.oneeall.android.myfinalproject.Adapter.FavAdapter;
import com.oneeall.android.myfinalproject.Adapter.MoviesAdapter;
import com.oneeall.android.myfinalproject.apiservice.MoviesApiService;
import com.oneeall.android.myfinalproject.modelPojo.Favorite;
import com.oneeall.android.myfinalproject.modelPojo.Movie;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    private RecyclerView recyclerView;
    private MoviesAdapter moviesAdapter;
    private FavAdapter favAdapter;
    private TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        setHasOptionsMenu(true);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));

        moviesAdapter = new MoviesAdapter(getContext());
        recyclerView.setAdapter(moviesAdapter);

        favAdapter = new FavAdapter(getContext());


        /*List<Movie> movies = new ArrayList<>();
        for (int i = 0; i < 25; i++){
            movies.add(new Movie());
        }

        moviesAdapter.setMovieList(movies);*/

        getPopularMovies();
        return view;
    }

    private void getPopularMovies() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://api.themoviedb.org/3")
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addEncodedQueryParam("api_key", "ccb27098ad69f758377a588b05654c6b");
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        MoviesApiService service = restAdapter.create(MoviesApiService.class);

        service.getPopularMovies(new Callback<Movie.MovieResult>() {
            @Override
            public void success(Movie.MovieResult movieResult, Response response) {
                moviesAdapter.setMovieList(movieResult.getResults());
                //Toast.makeText(getContext(), movieResult.toString() , Toast.LENGTH_SHORT).show();
                //Log.d("response 1 ", movieResult.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();

            }
        });




    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            getVoteCount();
            return true;
        }

        else if (id == R.id.action_popular){

            getPopularMovies();
            return true;

        }

        else if (id == R.id.action_fav){

            getFavoriteList();
        }

        return super.onOptionsItemSelected(item);
    }

    private void getFavoriteList() {
        List<Favorite> fav = SugarRecord.listAll(Favorite.class);

        favAdapter.setFavList(fav);
        recyclerView.setAdapter(favAdapter);

    }

    private void getVoteCount() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://api.themoviedb.org/3")
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addEncodedQueryParam("api_key", "ccb27098ad69f758377a588b05654c6b");
                        request.addQueryParam("sort_by","vote_average.desc");
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        MoviesApiService service = restAdapter.create(MoviesApiService.class);

        service.getVoteCounts(new Callback<Movie.MovieResult>() {
            @Override
            public void success(Movie.MovieResult movieResult, Response response) {
                moviesAdapter.setMovieList(movieResult.getResults());
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }
}

