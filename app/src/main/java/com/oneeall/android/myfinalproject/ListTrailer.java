package com.oneeall.android.myfinalproject;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.oneeall.android.myfinalproject.Adapter.TrailerAdapter;
import com.oneeall.android.myfinalproject.apiservice.MoviesApiService;
import com.oneeall.android.myfinalproject.modelPojo.Trailer;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ListTrailer extends Activity {

    private List<Trailer> TrailerList;
    ListView listView;

    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_trailer);


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int widht = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (widht * .70), (int) (height * .70));



        id = getIntent().getExtras().getInt("id");

        listView = (ListView) findViewById(R.id.listView);
        getMovieTrailers();
    }

    private void getMovieTrailers() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://api.themoviedb.org/3")
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addEncodedQueryParam("api_key", "ccb27098ad69f758377a588b05654c6b");
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        MoviesApiService service = restAdapter.create(MoviesApiService.class);

        service.getMovieTrailer(id, new Callback<Trailer.TrailerResult>() {
            @Override
            public void success(Trailer.TrailerResult trailerResult, Response response) {
                TrailerList = new ArrayList<Trailer>();
                TrailerList.addAll(trailerResult.getResults());
                TrailerAdapter tAdapter = new TrailerAdapter(getApplicationContext(), R.layout.row_trailer, (ArrayList<Trailer>) TrailerList);
                listView.setAdapter(tAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();

            }
        });
    }
}
