package com.oneeall.android.myfinalproject.apiservice;

import com.oneeall.android.myfinalproject.modelPojo.Movie;
import com.oneeall.android.myfinalproject.modelPojo.Trailer;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by oneal on 06/05/16.
 */
public interface MoviesApiService {

    @GET("/movie/popular")
    void  getPopularMovies(Callback<Movie.MovieResult> cb);

    @GET("/discover/movie")
    void getVoteCounts(Callback<Movie.MovieResult>cb1);

    @GET("/movie/{id}/videos")
    void getMovieTrailer(@Path("id") int id, Callback<Trailer.TrailerResult> response);
}
