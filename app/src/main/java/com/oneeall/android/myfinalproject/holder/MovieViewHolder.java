package com.oneeall.android.myfinalproject.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.oneeall.android.myfinalproject.R;

/**
 * Created by oneal on 06/05/16.
 */
public class MovieViewHolder extends RecyclerView.ViewHolder {

    public ImageView imageView;
    public MovieViewHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView.findViewById(R.id.imageView);
    }

}
