package com.oneeall.android.myfinalproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oneeall.android.myfinalproject.MovieDetailActivity;
import com.oneeall.android.myfinalproject.R;
import com.oneeall.android.myfinalproject.holder.MovieViewHolder;
import com.oneeall.android.myfinalproject.modelPojo.Favorite;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oneal on 10/05/16.
 */
public class FavAdapter extends RecyclerView.Adapter<MovieViewHolder>  {

    private List<Favorite> favList;
    private LayoutInflater layoutInflater;
    private Context mContext;


    public FavAdapter(Context context) {
        this.mContext = context;
        this.layoutInflater = layoutInflater.from(context);
        this.favList = new ArrayList<>();
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.row_movie, parent, false);
        final MovieViewHolder viewHolder = new MovieViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = viewHolder.getAdapterPosition();


                Intent intent = new Intent(mContext, MovieDetailActivity.class);
                intent.putExtra(MovieDetailActivity.EXTRA_FAV, favList.get(position));

                mContext.startActivity(intent);
            }
            });
            return viewHolder;
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        Favorite favorite = favList.get(position);
        Picasso.with(mContext).load(favorite.getPoster()).placeholder(R.color.colorPrimaryDark).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return (favList== null) ? 0 : favList.size();
    }

    public void setFavList(List<Favorite> favList){
        this.favList.clear();
        this.favList.addAll(favList);
        notifyDataSetChanged();
    }
}
