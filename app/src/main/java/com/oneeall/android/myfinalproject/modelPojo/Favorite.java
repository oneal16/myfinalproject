package com.oneeall.android.myfinalproject.modelPojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarApp;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

import java.util.List;

/**
 * Created by oneal on 10/05/16.
 */

public class Favorite extends SugarRecord implements Parcelable
{

    //private long id;
        String title;
     String poster;
     String description;
     String backdrop;
     String user_rating;
     String release_date;

    int ids;

    public Favorite() {
    }

    public Favorite(int ids, String title, String poster, String description, String backdrop, String user_rating, String release_date){
        this.ids = ids;
        this.title = title;
        this.poster = poster;
        this.description = description;
        this.backdrop = backdrop;
        this.user_rating = user_rating;
        this.release_date = release_date;
    }


    protected Favorite(Parcel in){
        title = in.readString();
        poster = in.readString();
        description = in.readString();
        backdrop = in.readString();
        user_rating = in.readString();
        release_date = in.readString();
        ids = in.readInt();
    }

    public static final Creator<Favorite> CREATOR = new Creator<Favorite>() {
        @Override
        public Favorite createFromParcel(Parcel source) {
            return new Favorite(source);
        }

        @Override
        public Favorite[] newArray(int size) {
            return new Favorite[size];
        }
    };



    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIds() {
        return ids;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser_rating() {
        return user_rating;
    }

    public void setUser_rating(String user_rating) {
        this.user_rating = user_rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(poster);
        dest.writeString(description);
        dest.writeString(backdrop);
        dest.writeString(user_rating);
        dest.writeString(release_date);
        dest.writeInt(ids);

    }

    public static class FavResult{
        private List<Favorite> results;
        public List<Favorite> getResults() {
            return results;
        }
    }
}
